#!/bin/bash

source "$@"

###############################################
#     Application Variables -  DO NOT EDIT    #
###############################################
# Day of the week;
DAYOFWEEK=`date +"%d"`
# Folder for all daily backups
DAILYBACKUPDIR=$BACKUP_DIR/SERVERS_BACKUP
# Name of directory to create for current backup
TODAYSBACKUPDIR=$DAILYBACKUPDIR/$DAYOFWEEK
# directory to store last weeks data
ARCHIVEDDATADIR=$BACKUP_DIR/BACKUPS_GUARDADOS
# Location of a file to hold the date stamp of last level 0 backup
L0DATESTAMP=$BACKUP_DIR/.level0_datestamp
# Do I really need to explain this one ;-)
NOW=`date`
# Log dir
LOGDIR=$BACKUP_DIR/REGISTROS
# Svript name
SCRIPTNAME="Simple Bash Backup Script"

# If user choose verbose, set the verbose command. Otherwise leave empty
if [ $VERBOSE -eq 1 ]
then
    VERBOSECOMMAND="--verbose"
fi
# Logfile
LOGFILE=$LOGDIR/`date +"%m%d%Y_%s"`.log


######## Do some error checking #########
# Does backup dir exist?
if [ ! -d $BACKUP_DIR ]
then
    #Send Email and Exit
    if [ $EMAIL -eq 1 ]
    then
        echo "El directorio indicado para el backup $BACKUP_DIR no existe. La operacion se a cancelado." | mail -s "$EMAILSUBJECT" $EMAILADDRESS
    fi
    echo "El directorio indicado para el backup $BACKUP_DIR no existe. La operacion se a cancelado."
    exit 1
fi
# Does the daily backup dir exist? If not, create it.
if [ ! -d $DAILYBACKUPDIR ]
then
    mkdir $DAILYBACKUPDIR
fi

# Does the daily backup dir exist? If not, create it.
if [ ! -d $LOGDIR ]
then
    mkdir $LOGDIR
fi


####### Redirect Output to a logfile and screen - Couldnt get tee to work
exec 3>&1                         # create pipe (copy of stdout)
exec 1>$LOGFILE                   # direct stdout to file
exec 2>&1                         # uncomment if you want stderr too
if [ $QUIET -eq 0 ]
then tail -f $LOGFILE >&3 &     # run tail in bg
fi


######## DO SOME PRINTING ###############
echo " "
echo "#######################################################################"
echo "$SCRIPTNAME "
echo "Version $VERSION"
echo "$COPYRIGHT"
echo " "
echo "Host: $HOSTNAME "
echo "Start Time: $NOW"
echo "#######################################################################"
echo " "
echo " "
echo " "

#ignoring files or directories
function ignore_files {
    IGNORE_FILES=""
    for IGNORE_FILE in $IGNORE
    do
        IGNORE_FILES+="--exclude=$IGNORE_FILE "
    done
}

######## Run Backup #########
#if day is LEVEL0DAY do full backup
if test $DAYOFWEEK -eq $LEVEL0DAY
then
    ignore_files
    LEVEL=0
    #we need to archive the last full backup to the weekold dir
    #make sure the week-old dir exists
    if test -d $ARCHIVEDDATADIR
    then
        #remove old data unless KEEPALL is set to 1
        if test $KEEPALL -eq 0
        then rm -Rf $ARCHIVEDDATADIR/*
        fi
    else
        #the week-old data dir didnt exist; create it
        mkdir $ARCHIVEDDATADIR
        chmod 700 $ARCHIVEDDATADIR
    fi
    #move the last full backup to the weekold dir
    mv -f $TODAYSBACKUPDIR/* $ARCHIVEDDATADIR > /dev/null 2>&1
    #remove all daily backups since they are simply incrementals on the bu we just archived
    rm -Rf $DAILYBACKUPDIR/*
    #make todays dir since we just blew it away
    mkdir $TODAYSBACKUPDIR
    #do FULL backup for each filesystem specified
    for BACKUPFILES in $FILESYSTEMS
    do
        #Create the filename; replace / with .
        WITHOUTSLASHES=`echo $BACKUPFILES | tr "/" "."`
        WITHOUTLEADINGDOT=`echo $WITHOUTSLASHES | cut -b2-`
        OUTFILENAME=$WITHOUTLEADINGDOT-$HOSTNAME.`date +"%m%d%Y_%s"`.tar
        OUTFILE=$TODAYSBACKUPDIR/$OUTFILENAME
        STARTTIME=`date`
        echo " "
        echo " "
        echo "#######################################################################"
        echo "$STARTTIME: Creando el backup nivel $LEVEL del directorio $BACKUPFILES al archivo $OUTFILE"
        echo "#######################################################################"
        tar --create $VERBOSECOMMAND \
            --file $OUTFILE \
            $IGNORE_FILES \
            --label "Level-$LEVEL Backup ${NOW}" \
            $BACKUPFILES
        if test $COMPRESS -eq 1
        then
            #gzip it
            gzip -$COMPRESSLEVEL $VERBOSECOMMAND $OUTFILE
            rm -f $OUTFILE
        fi
    done
    # Does the timestamp file exist? If not, create it.
    if [ ! -w $L0DATESTAMP ]
    then
        touch $L0DATESTAMP
    fi
    #record full backup timestamp
    echo $NOW > $L0DATESTAMP
    MADEIT=true
else
    MADEIT=false
    # *TODO* uncomment this block to enable daily backups
    ##we should do an incremental backup
    #ignore_files
    #LEVEL=1
    ## Does todays backup dir exist? If not, create it.
    #if [ ! -d $TODAYSBACKUPDIR ]
    #then
        #mkdir $TODAYSBACKUPDIR
    #fi
    ## Does the timestamp file exist? If not, create it.
    #if [ ! -w $L0DATESTAMP ]
    #then
        #touch $L0DATESTAMP
        #echo "1969-12-31" > $L0DATESTAMP
    #fi
    ##get date of last full backup
    #LAST=`cat $L0DATESTAMP`
    ##Do a level 1 Backup for each filesystem specified
    #for BACKUPFILES in $FILESYSTEMS
    #do
        ##Create the filename; replace / with .
        #WITHOUTSLASHES=`echo $BACKUPFILES | tr "/" "."`
        #WITHOUTLEADINGDOT=`echo $WITHOUTSLASHES | cut -b2-`
        #OUTFILENAME=$WITHOUTLEADINGDOT-$HOSTNAME.`date +"%m%d%Y_%s"`.tar
        #OUTFILE=$TODAYSBACKUPDIR/$OUTFILENAME
        #STARTTIME=`date`
        #echo " "
        #echo " "
        #echo "#######################################################################"
        #echo "$STARTTIME: Creando el backup nivel $LEVEL del directorio $BACKUPFILES al archivo $OUTFILE"
        #echo "#######################################################################"
        #tar --create $VERBOSECOMMAND \
            #--file $OUTFILE \
            #$IGNORE_FILES \
            #--after-date "$LAST" \
            #--label "Level-$LEVEL Backup from $LAST to $NOW" \
            #$BACKUPFILES
        #if test $COMPRESS -eq 1
        #then
            ##gzip it
            #gzip -$COMPRESSLEVEL $VERBOSECOMMAND $OUTFILE
            #rm -f $OUTFILE
        #fi
    #done
fi

SCRIPTFINISHTIME=`date`
echo " "
echo " "
echo " "
echo " "
echo "#######################################################################"
echo "Finalizado en: $SCRIPTFINISHTIME"
echo " "
echo "  NOTA: Siempre examine la salida de este script en busca de errores."
echo "        De la misma manera los archivos de backup y valide que es posible"
echo "        restaurar desde ellos."
echo "#######################################################################"

#email notification

if [ $EMAIL -eq 1 ]
then
    if $MADEIT
    then
        cat $LOGFILE | mail -s "$EMAILSUBJECT" $EMAILADDRESS
    fi
fi


#*TODO*: Fix stdout and stderr to go to console instead of og file since we are
#about to compress the log file
exec 1>&3
exec 2>&3

#compress log file?
if test $COMPRESSLOG -eq 1
then
    #gzip it
    gzip -$COMPRESSLEVEL $LOGFILE  > /dev/null 2>&1
    rm -f $LOGFILE  > /dev/null 2>&1
fi

exit 0
